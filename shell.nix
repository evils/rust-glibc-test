{ pkgs ? import <nixpkgs> { } }:

with pkgs;

stdenv.mkDerivation {
  name = "rustenv";
  nativeBuildInputs = [
    rustup
  ];
  RUST_BACKTRACE = 1;
  # hint, use lorri shell if you want to keep your shell and prompt
}
